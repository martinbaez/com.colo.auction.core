package com.colo.auction.dataSource.core.mongo;

import com.colo.auction.documents.mongo.user.AuctionUser;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author martin.baez
 * @date 3/08/19
 */
public interface UserRepository extends MongoRepository<AuctionUser,String> {
}
