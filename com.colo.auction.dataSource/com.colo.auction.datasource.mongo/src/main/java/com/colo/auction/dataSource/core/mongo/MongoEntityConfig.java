package com.colo.auction.dataSource.core.mongo;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * @author martin.baez
 * @date 2/08/19
 */
@Configuration
@EnableMongoRepositories(basePackageClasses = {AuctionRepository.class})
public class MongoEntityConfig {


}
