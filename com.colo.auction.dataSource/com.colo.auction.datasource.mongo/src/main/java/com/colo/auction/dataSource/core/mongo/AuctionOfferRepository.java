package com.colo.auction.dataSource.core.mongo;

import com.colo.auction.documents.mongo.offer.Offer;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AuctionOfferRepository extends MongoRepository<Offer,String> {
}
