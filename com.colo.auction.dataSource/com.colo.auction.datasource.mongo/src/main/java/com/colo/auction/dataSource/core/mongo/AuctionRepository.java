package com.colo.auction.dataSource.core.mongo;

import com.colo.auction.documents.mongo.auction.Auction;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author martin.baez
 * @date 17/06/19
 */
public interface AuctionRepository extends MongoRepository<Auction,String> {


}
