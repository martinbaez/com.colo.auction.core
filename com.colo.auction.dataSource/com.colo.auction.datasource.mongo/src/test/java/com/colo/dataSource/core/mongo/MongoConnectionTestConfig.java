package com.colo.dataSource.core.mongo;

import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.context.annotation.Configuration;

/**
 * @author martin.baez
 * @date 2/08/19
 */
@Configuration
@DataMongoTest
//@ContextConfiguration(classes = CustomSimpleMongoConfig.class)
//@DataMongoTest(excludeAutoConfiguration= {EmbeddedMongoAutoConfiguration.class})
public class MongoConnectionTestConfig {
}
