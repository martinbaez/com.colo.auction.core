package com.colo.dataSource.core.mongo.auction;

import com.colo.auction.dataSource.core.mongo.AuctionRepository;
import com.colo.auction.documents.mongo.auction.Auction;
import com.colo.auction.domain.core.test.MockAuctionFactory;
import com.colo.dataSource.core.mongo.MongoConnectionTestConfig;
import com.colo.dataSource.core.mongo.MongoRepositoryTestConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * @author martin.baez
 * @date 17/06/19
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {MongoRepositoryTestConfig.class, MongoConnectionTestConfig.class})
public class AuctionRepositoryTest {

    @Autowired
    private AuctionRepository auctionRepository;

   @Test
    public void testAuctionRepository(){
       Auction auction  = MockAuctionFactory.buildAuctionWithSomeOffer();

       this.auctionRepository.save(auction);

       Auction auction2  = MockAuctionFactory.buildAuctionWithSomeOffer();

       this.auctionRepository.save(auction2);

       List<Auction> auctions = this.auctionRepository.findAll();

       this.auctionRepository.deleteAll();
    }

}
