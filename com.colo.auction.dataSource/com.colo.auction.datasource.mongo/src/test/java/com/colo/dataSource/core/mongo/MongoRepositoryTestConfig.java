package com.colo.dataSource.core.mongo;

import com.colo.auction.dataSource.core.mongo.AuctionRepository;
import com.colo.auction.dataSource.core.mongo.UserRepository;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * @author martin.baez
 * @date 17/06/19
 * Uses Embedded MongoDB if de.flapdoodle.embed is found in the classpath
 *
 */
@Configuration
@EnableMongoRepositories(basePackageClasses = {AuctionRepository.class, UserRepository.class})
public class MongoRepositoryTestConfig {

}
