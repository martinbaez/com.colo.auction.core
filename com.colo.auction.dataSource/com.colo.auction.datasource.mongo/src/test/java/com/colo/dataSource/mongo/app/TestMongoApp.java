package com.colo.dataSource.mongo.app;

import com.mongodb.MongoClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.MongoTemplate;

/**
 * @author martin.baez
 * @date 20/06/19
 */
@SpringBootApplication(scanBasePackageClasses = { TestMongoApp.class },exclude = {HibernateJpaAutoConfiguration.class, DataSourceAutoConfiguration.class})
public class TestMongoApp implements CommandLineRunner {

    @Autowired
    private MongoClient client;

    @Override
    public void run(String... args) throws Exception {
         System.out.println(this.client.getCredentialsList());
        this.client.getDatabase("colo").createCollection("zapatos");

    }


    public static void main(String[] args) {
        SpringApplication.run(TestMongoApp.class, args);
    }

        public @Bean
        MongoClient mongo() throws Exception {
            return new MongoClient("localhost");
        }

        public @Bean
        MongoTemplate mongoTemplate() throws Exception {
            return new MongoTemplate(mongo(), "colo");
        }

}
