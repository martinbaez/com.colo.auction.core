package com.colo.dataSource.core.mongo.user;

import com.colo.auction.dataSource.core.mongo.UserRepository;
import com.colo.auction.documents.mongo.user.AuctionUser;
import com.colo.auction.domain.core.test.MockUserFactory;
import com.colo.dataSource.core.mongo.MongoConnectionTestConfig;
import com.colo.dataSource.core.mongo.MongoRepositoryTestConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author martin.baez
 * @date 3/08/19
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {MongoRepositoryTestConfig.class, MongoConnectionTestConfig.class})
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void testUserRepository(){
        AuctionUser auction  = MockUserFactory.buildUser();

        this.userRepository.save(auction);

        this.userRepository.deleteAll();
    }

}
