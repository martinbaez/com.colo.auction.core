#!/usr/bin/env bash

function get_auction_id() {
        cat $1 | jq -r '.id'
}