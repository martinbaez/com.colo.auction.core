#!/usr/bin/env bash

function create_auction(){
 echo >> output
 do_create_auction "$1" $2 "$3" "$4" $5 >> output
}

function retrieve_auction(){
 echo >> output
 do_retrieve_auction "$1" >> output
}


function do_create_auction(){

curl --silent -X POST "http://localhost:8080/auction-core/auctions" \
 -H  "accept: */*" \
 -H  "Content-Type: application/json" \
 -d   @- << EOF

        {
          "buyer": "$1",
          "newAuctionObject": {
            "count": $2,
            "description": "$3",
            "name": "$4"
          },
          "price": $5
        }
EOF

}

function do_retrieve_auction(){
 id_auction=$1
 curl --silent -X GET "http://localhost:8080/auction-core/auctions?id=$id_auction" -H  "accept: */*"
}