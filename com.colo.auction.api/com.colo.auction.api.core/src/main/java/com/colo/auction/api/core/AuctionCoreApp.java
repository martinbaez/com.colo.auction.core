package com.colo.auction.api.core;

import com.colo.service.core.ServiceConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;

/**
 * @author martin.baez
 * @date 29/06/19
 */
@SpringBootApplication(scanBasePackageClasses = {AuctionCoreApp.class, ServiceConfig.class},
        exclude = {DataSourceAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
public class AuctionCoreApp {

    public static void main(String[] args) {
        SpringApplication.run(AuctionCoreApp.class, args);
    }




}
