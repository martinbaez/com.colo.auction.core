package com.colo.auction.api.core.impl;

import com.colo.auction.api.core.IOfferEndpoint;
import com.colo.service.core.dto.offer.UpdateOffer;
import com.colo.service.core.IOfferService;
import com.colo.service.core.dto.offer.NewOffer;
import com.colo.service.core.dto.offer.RetrieveOffer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author martin.baez
 * @date 10/09/19
 */
@RestController
public class OfferEndpoint implements IOfferEndpoint {

    @Autowired
    private IOfferService auctionOfferService;

    @Override
    @PostMapping("offers")
    public ResponseEntity<RetrieveOffer> addOffer(String auctionId, @RequestBody NewOffer newOffer) {
        RetrieveOffer offer = this.auctionOfferService.addOffer(auctionId,newOffer);
        return ResponseEntity.ok(offer);
    }

    @Override
    @PatchMapping("offers/{offerId}")
    public ResponseEntity<RetrieveOffer> updateOffer(String offerId,@RequestBody UpdateOffer offer) {
        RetrieveOffer retrieveOffer = this.auctionOfferService.updateOffer(offerId,offer);
        return null;
    }

    @Override
    @GetMapping("offers/{offerId}")
    public ResponseEntity<RetrieveOffer> findOffer(@PathVariable String offerId) {
        RetrieveOffer offer = this.auctionOfferService.findOffer(offerId);
        return ResponseEntity.ok(offer);
    }

    @Override
    @GetMapping("offers")
    public ResponseEntity<List<RetrieveOffer>> findOffer() {
        List<RetrieveOffer> offers = this.auctionOfferService.findOffer();
        return ResponseEntity.ok(offers);
    }
}
