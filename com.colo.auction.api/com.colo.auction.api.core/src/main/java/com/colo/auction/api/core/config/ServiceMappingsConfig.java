package com.colo.auction.api.core.config;

import org.dozer.DozerBeanMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;

/**
 * @author martin.baez
 * @date 12/09/19
 */
@Configuration
public class ServiceMappingsConfig {

    private static String[] DOZZER_MAPPINGS = {
            "auction_2_retrieveAuction_dozer_mapping.xml",
            "user_2_retrieve_user_dozer_mapping.xml",
            "create_auction_2_auction_dozer_mapping.xml",
            "create_auction_object_2_auction_object_dozer_mapping.xml",
            "offer_2_retrieve_offer_dozer_mapping.xml"
    };


    @Bean
    public DozerBeanMapper dozerBeanMapper(){
        DozerBeanMapper mapper = new DozerBeanMapper(mappings());
        return mapper;
    }

    public static List<String> mappings(){
        return Arrays.asList(DOZZER_MAPPINGS);
    }
}
