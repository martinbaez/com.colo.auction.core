package com.colo.auction.api.core;

import com.colo.service.core.dto.auction.RetrieveAuction;
import com.colo.service.core.dto.user.RetrieveUser;

import java.util.List;

public interface IUserEndpoint {


    RetrieveUser findById(String userId);

    List<RetrieveAuction> retrieveAuctions(String userId);

    List<RetrieveAuction> retrieveOffers(String userId);

    List findAll();


}
