package com.colo.auction.api.core.impl;

import com.colo.auction.api.core.IAuctionEndpoint;
import com.colo.auction.api.core.exception.AuctionNotFoundException;
import com.colo.service.core.IAuctionService;
import com.colo.service.core.dto.auction.UpdateAuction;
import com.colo.service.core.exceptions.InvalidAuctionException;
import com.colo.service.core.dto.auction.NewAuction;
import com.colo.service.core.dto.auction.RetrieveAuction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author martin.baez
 * @date 28/06/19
 */
@RestController
public class AuctionEndpoint implements IAuctionEndpoint {

    @Autowired
    private IAuctionService auctionService;

    public AuctionEndpoint() {

    }

    @Override
    @GetMapping("auctions/{id}")
    public ResponseEntity<RetrieveAuction> findAuction(@PathVariable String id) {
        try {
            RetrieveAuction auction = this.auctionService.findById(id);
            return ResponseEntity.ok(auction);
        }
        catch (InvalidAuctionException ex){
            throw new AuctionNotFoundException();
        }

    }

    @Override
    @GetMapping("auctions")
    public ResponseEntity<List<RetrieveAuction>> findAuction() {
        try {
            List<RetrieveAuction> auctions = this.auctionService.findAll();
            return ResponseEntity.ok(auctions);
        }
        catch (InvalidAuctionException ex){
            throw new AuctionNotFoundException();
        }

    }

    @Override
    @PostMapping("auctions")
    public ResponseEntity<RetrieveAuction> createAuction(@RequestBody NewAuction pCreateAuction) {
         RetrieveAuction auction = this.auctionService.createAuction(pCreateAuction);
        return ResponseEntity.ok(auction);
    }


    @Override
    @PatchMapping("auctions/{id}")
    public ResponseEntity<RetrieveAuction> updateAuction(@PathVariable String auctionId,@RequestBody UpdateAuction updateAuction) {
        RetrieveAuction auction = this.auctionService.updateAuction(auctionId,updateAuction);
        return ResponseEntity.ok(auction);
    }




}
