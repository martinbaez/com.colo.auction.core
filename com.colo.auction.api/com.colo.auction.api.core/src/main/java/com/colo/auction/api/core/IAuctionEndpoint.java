package com.colo.auction.api.core;

import com.colo.service.core.dto.auction.NewAuction;
import com.colo.service.core.dto.auction.UpdateAuction;
import com.colo.service.core.dto.offer.NewOffer;
import com.colo.service.core.dto.auction.RetrieveAuction;
import org.hibernate.sql.Update;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author martin.baez
 * @date 28/06/19
 */

public interface IAuctionEndpoint {

    ResponseEntity<RetrieveAuction> findAuction(String id);

    ResponseEntity<List<RetrieveAuction>> findAuction();

    ResponseEntity<RetrieveAuction> createAuction(NewAuction pCreateAuction);

    ResponseEntity<RetrieveAuction> updateAuction(String auctionId,@RequestBody UpdateAuction pUpdateAuction);


}
