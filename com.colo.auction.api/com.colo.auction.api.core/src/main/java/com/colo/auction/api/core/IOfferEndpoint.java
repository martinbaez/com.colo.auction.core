package com.colo.auction.api.core;

import com.colo.service.core.dto.offer.NewOffer;
import com.colo.service.core.dto.offer.RetrieveOffer;
import com.colo.service.core.dto.offer.UpdateOffer;
import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * @author martin.baez
 * @date 10/09/19
 */

public interface IOfferEndpoint {

    ResponseEntity<RetrieveOffer> addOffer(String auctionId, NewOffer offer);

    ResponseEntity<RetrieveOffer> updateOffer(String offerId, UpdateOffer offer);

    ResponseEntity<RetrieveOffer> findOffer(String auctionId);

    ResponseEntity<List<RetrieveOffer>> findOffer();


}
