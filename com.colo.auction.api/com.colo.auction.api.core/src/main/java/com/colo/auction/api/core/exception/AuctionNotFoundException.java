package com.colo.auction.api.core.exception;

import com.colo.auction.api.core.exception.EndpointException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author martin.baez
 * @date 19/08/19
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class AuctionNotFoundException extends EndpointException {
}
