package com.colo.auction.api.core.impl;

import com.colo.auction.api.core.IUserEndpoint;
import com.colo.service.core.IUserService;
import com.colo.service.core.dto.auction.RetrieveAuction;
import com.colo.service.core.dto.user.RetrieveUser;
import com.colo.service.core.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author martin.baez
 * @date 12/09/19
 */
@RestController
public class UserEndpoint implements IUserEndpoint {

    @Autowired
    private IUserService userService;

    @Override
    @GetMapping("users/{userId}")
    public RetrieveUser findById(String userId) {
        return (RetrieveUser) this.userService.findById(userId);
    }

    @Override
    @GetMapping("users")
    public List<RetrieveUser> findAll() {
        return (List<RetrieveUser>) this.userService.findAll();
    }


    @Override
    @GetMapping("users/{userId}/auctions")
    public List<RetrieveAuction> retrieveAuctions(@PathVariable String userId) {
        return null;
    }

    @Override
    @GetMapping("users/{userId}/offers")
    public List<RetrieveAuction> retrieveOffers(@PathVariable String userId) {
        return null;
    }
}
