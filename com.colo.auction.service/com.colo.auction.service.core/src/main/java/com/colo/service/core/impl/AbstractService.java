package com.colo.service.core.impl;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author martin.baez
 * @date 12/09/19
 */

public abstract class AbstractService<T,R> implements IService<T,R > {

    protected MongoRepository<T,String> repository;

    private Class<R> type=null;

    @Autowired
    private DozerBeanMapper mapper;


    public AbstractService() {
        this.type = getRetrieveClass();

    }

    public AbstractService(Class<R> type) {
        this.type = type;
    }

    @Override
    public R findById(String id) {
      T retrievedObject =  this.repository.findById(id).get();
      return this.convert(retrievedObject);
    }


    @Override
    public boolean exists(String id){
        return this.repository.existsById(id);
    }

    @Override
    public List<R> findAll() {
        List<T> all = this.repository.findAll();
        List<R> result = this.map(all);
        return result;
  }


    private List<R> map(List<T> objects) {
        return objects.stream().map( each -> this.convert(each)).collect(Collectors.toList());
    }

    public R convert(T t){
      return  this.mapper.map(t,type);
    }

    protected abstract Class getRetrieveClass();

}
