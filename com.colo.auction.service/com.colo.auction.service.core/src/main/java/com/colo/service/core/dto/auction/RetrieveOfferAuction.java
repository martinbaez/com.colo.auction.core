package com.colo.service.core.dto.auction;

import com.colo.auction.documents.mongo.auction.AuctionStatus;
import com.colo.service.core.dto.user.RetrieveUser;

/**
 * @author martin.baez
 * @date 11/09/19
 */

public class RetrieveOfferAuction {

    private String id;

    private Double price;

    private AuctionStatus status;

    private RetrieveUser buyer;

    private RetrieveAuctionObject auctionObject;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public AuctionStatus getStatus() {
        return status;
    }

    public void setStatus(AuctionStatus status) {
        this.status = status;
    }

    public RetrieveUser getBuyer() {
        return buyer;
    }

    public void setBuyer(RetrieveUser buyer) {
        this.buyer = buyer;
    }

    public RetrieveAuctionObject getAuctionObject() {
        return auctionObject;
    }

    public void setAuctionObject(RetrieveAuctionObject auctionObject) {
        this.auctionObject = auctionObject;
    }
}
