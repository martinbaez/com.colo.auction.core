package com.colo.service.core.impl;

import com.colo.auction.dataSource.core.mongo.AuctionRepository;
import com.colo.auction.documents.mongo.auction.Auction;
import com.colo.service.core.IAuctionService;
import com.colo.service.core.builder.auction.AuctionBuilder;
import com.colo.service.core.dto.auction.NewAuction;
import com.colo.service.core.dto.auction.RetrieveAuction;
import com.colo.service.core.dto.auction.UpdateAuction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author martin.baez
 * @date 17/06/19
 */
@Service
public class AuctionServiceImpl  extends AbstractService<Auction, RetrieveAuction>  implements IAuctionService {

    @Autowired
    private AuctionBuilder auctionBuilder;




    @Override
    public RetrieveAuction createAuction(NewAuction pCreateAuction) {
        Auction auctionCreated = this.getAuctionBuilder().map(pCreateAuction);
        this.repository.save(auctionCreated);
        RetrieveAuction auction = this.getAuctionBuilder().map(auctionCreated);
        return auction;
    }

    @Override
    public RetrieveAuction updateAuction(String auctionId, UpdateAuction pCreateAuction) {
        return null;
    }


    public AuctionBuilder getAuctionBuilder() {
        return auctionBuilder;
    }

    public void setAuctionBuilder(AuctionBuilder auctionBuilder) {
        this.auctionBuilder = auctionBuilder;
    }

    @Override
    protected Class getRetrieveClass() {
        return RetrieveAuction.class;
    }

    @Autowired
    public void setRepository(AuctionRepository auctionRepository){
        this.repository = auctionRepository;
    }

}
