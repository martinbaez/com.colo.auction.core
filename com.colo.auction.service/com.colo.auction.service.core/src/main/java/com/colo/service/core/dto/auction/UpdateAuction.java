package com.colo.service.core.dto.auction;

import com.colo.auction.documents.mongo.auction.AuctionStatus;

/**
 * @author martin.baez
 * @date 12/09/19
 */

public class UpdateAuction {

    private AuctionStatus status;

    private Double price;

    public AuctionStatus getStatus() {
        return status;
    }

    public void setStatus(AuctionStatus status) {
        this.status = status;
    }
}