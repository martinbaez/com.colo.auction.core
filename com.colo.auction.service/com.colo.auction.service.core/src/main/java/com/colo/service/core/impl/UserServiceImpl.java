package com.colo.service.core.impl;

import com.colo.auction.dataSource.core.mongo.UserRepository;
import com.colo.auction.documents.mongo.user.AuctionUser;
import com.colo.service.core.IUserService;
import com.colo.service.core.dto.user.RetrieveUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author martin.baez
 * @date 12/09/19
 */
@Service
public class UserServiceImpl extends AbstractService<AuctionUser,RetrieveUser> implements IUserService {


    @Autowired
    public void setRepository(UserRepository userRepository){
        this.repository = userRepository;
    }


    @Override
    protected Class getRetrieveClass() {
        return RetrieveUser.class;
    }


}
