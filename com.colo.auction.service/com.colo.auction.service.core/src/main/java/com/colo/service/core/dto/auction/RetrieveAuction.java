package com.colo.service.core.dto.auction;

import com.colo.auction.documents.mongo.auction.AuctionStatus;
import com.colo.service.core.dto.offer.RetrieveOffer;

import java.util.ArrayList;
import java.util.List;

/**
 * @author martin.baez
 * @date 4/08/19
 */
public class RetrieveAuction {

    private String id;

    private Double price;

    private AuctionStatus status;

    private String buyer;

    private RetrieveAuctionObject auctionObject;

    private List<RetrieveOffer> offers = new ArrayList<RetrieveOffer>();

    public RetrieveAuction() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public AuctionStatus getStatus() {
        return status;
    }

    public void setStatus(AuctionStatus status) {
        this.status = status;
    }

    public String getBuyer() {
        return buyer;
    }

    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public RetrieveAuctionObject getAuctionObject() {
        return auctionObject;
    }

    public void setAuctionObject(RetrieveAuctionObject auctionObject) {
        this.auctionObject = auctionObject;
    }

    public List<RetrieveOffer> getOffers() {
        return offers;
    }

    public void setOffers(List<RetrieveOffer> offers) {
        this.offers = offers;
    }
}
