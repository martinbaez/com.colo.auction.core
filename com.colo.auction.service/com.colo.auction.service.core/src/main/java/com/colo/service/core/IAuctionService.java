package com.colo.service.core;


import com.colo.auction.documents.mongo.auction.Auction;
import com.colo.service.core.dto.auction.NewAuction;
import com.colo.service.core.dto.auction.RetrieveAuction;
import com.colo.service.core.dto.auction.UpdateAuction;
import com.colo.service.core.impl.IService;

public interface IAuctionService extends IService<Auction, RetrieveAuction> {

    RetrieveAuction createAuction(NewAuction pCreateAuction);

    RetrieveAuction updateAuction(String auctionId,UpdateAuction pCreateAuction);
}
