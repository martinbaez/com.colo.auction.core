package com.colo.service.core.dto.auction;

import java.util.HashSet;
import java.util.Set;

/**
 * @author martin.baez
 * @date 3/08/19
 */

public class NewAuctionObject {

    private int count;

    private String name;
    
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }





}
