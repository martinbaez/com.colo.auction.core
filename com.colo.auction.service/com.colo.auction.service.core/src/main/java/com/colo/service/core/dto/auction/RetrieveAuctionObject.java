package com.colo.service.core.dto.auction;

/**
 * @author martin.baez
 * @date 4/08/19
 */

public class RetrieveAuctionObject {

    private int count;

    private String name;

    private String description;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
