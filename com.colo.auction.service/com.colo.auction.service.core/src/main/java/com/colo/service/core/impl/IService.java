package com.colo.service.core.impl;

import java.util.List;

public interface IService<T, R> {

    R findById(String id);

    boolean exists(String id);

    List<R> findAll();
}
