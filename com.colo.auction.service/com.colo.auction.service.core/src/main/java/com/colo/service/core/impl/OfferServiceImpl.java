package com.colo.service.core.impl;

import com.colo.auction.dataSource.core.mongo.AuctionOfferRepository;
import com.colo.auction.dataSource.core.mongo.AuctionRepository;
import com.colo.auction.dataSource.core.mongo.UserRepository;
import com.colo.auction.documents.mongo.auction.Auction;
import com.colo.auction.documents.mongo.offer.Offer;
import com.colo.auction.documents.mongo.offer.OfferStatus;
import com.colo.service.core.IOfferService;
import com.colo.service.core.builder.auction.AuctionBuilder;
import com.colo.service.core.dto.offer.NewOffer;
import com.colo.service.core.dto.offer.RetrieveOffer;
import com.colo.service.core.dto.offer.UpdateOffer;
import com.colo.service.core.exceptions.InvalidOfferException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author martin.baez
 * @date 10/09/19
 */
@Service
public class OfferServiceImpl implements IOfferService {

    @Autowired
    private AuctionRepository auctionRepository;

    @Autowired
    private AuctionOfferRepository auctionOfferRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuctionBuilder auctionBuilder;

    @Override
    public RetrieveOffer addOffer(String auctionId, NewOffer newOffer) {

        Auction auction =  this.auctionRepository.findById(auctionId).get();

        Offer offer = new Offer();
        offer.setPrice(newOffer.getPrice());
        offer.setDate(AuctionUtils.now());
        offer.setStatus(OfferStatus.SUBMITTED);
        offer.setAuction(auction);

        this.auctionOfferRepository.save(offer);

        auction.addOffer(offer);

        this.auctionRepository.save(auction);

        RetrieveOffer result = this.getAuctionBuilder().map(offer);

        return result;
    }

    @Override
    public List<RetrieveOffer> findOffer() {

       List<Offer> offers = this.auctionOfferRepository.findAll();
       List<RetrieveOffer> result = this.auctionBuilder.mapOffers(offers);
      return result;
    }

    @Override
    public RetrieveOffer findOffer(String offerId) {
        if (this.auctionOfferRepository.existsById(offerId)) {
            Offer offer = this.auctionOfferRepository.findById(offerId).get();
            return this.auctionBuilder.map(offer);
        }
        else {
            throw new InvalidOfferException();

        }
    }

    @Override
    public RetrieveOffer updateOffer(String offerId, UpdateOffer updateOffer) {
        Offer offer= this.doFindOffer(offerId);

        if (!offer.getPrice().equals(updateOffer.getPrice())) {
            offer.setPrice(updateOffer.getPrice());
        }

        if (!offer.getStatus().equals(updateOffer.getStatus())) {
            offer.setStatus(updateOffer.getStatus());
        }

        this.auctionOfferRepository.save(offer);

        return this.auctionBuilder.map(offer);

    }

    private AuctionBuilder getAuctionBuilder() {
        return auctionBuilder;
    }

    public void setAuctionBuilder(AuctionBuilder auctionBuilder) {
        this.auctionBuilder = auctionBuilder;
    }


    private Offer doFindOffer(String offerId){
        if (this.auctionOfferRepository.existsById(offerId)) {
            return this.auctionOfferRepository.findById(offerId).get();
        }
        else {
            throw new InvalidOfferException();
        }
    }
}
