package com.colo.service.core.dto.offer;

import com.colo.auction.documents.mongo.offer.OfferStatus;

/**
 * @author martin.baez
 * @date 12/09/19
 */

public class UpdateOffer {

    private Double price;

    private OfferStatus status;


    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public OfferStatus getStatus() {
        return status;
    }

    public void setStatus(OfferStatus status) {
        this.status = status;
    }
}
