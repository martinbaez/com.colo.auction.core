package com.colo.service.core.builder.auction;

import com.colo.auction.documents.mongo.auction.Auction;
import com.colo.auction.documents.mongo.offer.Offer;
import com.colo.auction.documents.mongo.user.AuctionUser;
import com.colo.service.core.dto.auction.NewAuction;
import com.colo.service.core.dto.auction.RetrieveAuction;
import com.colo.service.core.dto.offer.RetrieveOffer;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author martin.baez
 * @date 3/08/19
 */
@Component
public class AuctionBuilder {


    private static String[] mappings = {
            "auction_2_retrieveAuction_dozer_mapping.xml",
            "user_2_retrieve_user_dozer_mapping.xml",
            "create_auction_2_auction_dozer_mapping.xml",
            "create_auction_object_2_auction_object_dozer_mapping.xml",
            "offer_2_retrieve_offer_dozer_mapping.xml"
    };


    private DozerBeanMapper auctionMapper = new DozerBeanMapper();



    public AuctionBuilder() {

        this.auctionMapper = new DozerBeanMapper(AuctionBuilder.mappings());


    }


    public Auction map(NewAuction pCreateAuction){
       Auction auction = this.auctionMapper.map(pCreateAuction,Auction.class);
        //auction.setId(UUID.randomUUID().toString());
        auction.validateState();
        return auction;
    }

    public RetrieveAuction map(Auction auction) {
       return this.auctionMapper.map(auction,RetrieveAuction.class);
    }

    public static List<String> mappings(){
        return Arrays.asList(mappings);
    }

    public List<RetrieveAuction> map(List<Auction> auctions) {
        return auctions.stream().map( each -> this.map(each)).collect(Collectors.toList());
    }

    public RetrieveOffer map(Offer offer){
        RetrieveOffer result = this.auctionMapper.map(offer, RetrieveOffer.class);
        return result;


    }

    public List<RetrieveOffer> mapOffers(List<Offer> offers){
        return offers.stream().map( each -> this.map(each)).collect(Collectors.toList());



    }

    public Object map(Object object,Class objectClass) {
        return this.map(object,objectClass);
    }
}
