package com.colo.service.core.dto.auction;

/**
 * @author martin.baez
 * @date 3/08/19
 */

public class NewAuction {

    private String buyer;

    private double price;

    private NewAuctionObject newAuctionObject = new NewAuctionObject();


    public String getBuyer() {
        return buyer;
    }

    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public void setNewAuctionObject(NewAuctionObject newAuctionObject) {
        this.newAuctionObject = newAuctionObject;
    }

    public NewAuctionObject getNewAuctionObject() {
        return newAuctionObject;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
