package com.colo.service.core.dto.offer;

/**
 * @author martin.baez
 * @date 20/08/19
 */

public class NewOffer {

    private Double price;

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

}
