package com.colo.service.core;

import com.colo.auction.dataSource.core.mongo.MongoEntityConfig;
import com.colo.service.core.impl.AuctionServiceImpl;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * @author martin.baez
 * @date 2/08/19
 */
@Configuration
@EnableMongoRepositories(basePackageClasses = {AuctionServiceImpl.class})
@Import(MongoEntityConfig.class)
public class ServiceConfig {
}
