package com.colo.service.core.dto.offer;

import com.colo.auction.documents.mongo.offer.OfferStatus;
import com.colo.service.core.dto.auction.RetrieveAuction;
import com.colo.service.core.dto.auction.RetrieveOfferAuction;

import java.util.Date;


/**
 * @author martin.baez
 * @date 20/08/19
 */

public class RetrieveOffer {

    private String offerId;

    private RetrieveOfferAuction auction;

    private Double price;

    private Date date;

    private OfferStatus status;


    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public OfferStatus getStatus() {
        return status;
    }

    public void setStatus(OfferStatus status) {
        this.status = status;
    }

    public RetrieveOfferAuction getAuction() {
        return auction;
    }

    public void setAuction(RetrieveOfferAuction auction) {
        this.auction = auction;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }
}
