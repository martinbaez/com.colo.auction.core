package com.colo.service.core;

import com.colo.service.core.dto.offer.NewOffer;
import com.colo.service.core.dto.offer.RetrieveOffer;
import com.colo.service.core.dto.offer.UpdateOffer;

import java.util.List;

public interface IOfferService {

    RetrieveOffer addOffer(String auctionId, NewOffer newOffer);

    List<RetrieveOffer> findOffer();

    RetrieveOffer findOffer(String offerId);

    RetrieveOffer updateOffer(String offerId, UpdateOffer offer);
}
