package com.colo.service.core;

import com.colo.auction.documents.mongo.user.AuctionUser;
import com.colo.service.core.dto.user.RetrieveUser;
import com.colo.service.core.impl.IService;

/**
 * @author martin.baez
 * @date 12/09/19
 */

public interface IUserService extends IService<AuctionUser, RetrieveUser> {

}
