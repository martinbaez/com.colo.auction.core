package com.colo.service.core.impl;

import java.sql.Timestamp;

/**
 * @author martin.baez
 * @date 10/09/19
 */

public class AuctionUtils {
    public static Timestamp now() {
        return new Timestamp(System.currentTimeMillis());
    }
}
