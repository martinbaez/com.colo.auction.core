package com.colo.service.core.dto.auction.retrieve;

import com.colo.auction.documents.mongo.auction.Auction;
import com.colo.auction.domain.core.test.MockAuctionFactory;
import com.colo.service.core.builder.auction.AuctionBuilder;
import com.colo.service.core.dto.auction.RetrieveAuction;
import org.dozer.DozerBeanMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author martin.baez
 * @date 4/08/19
 */
public class MapAuctionDtoTest {

    private DozerBeanMapper mapper;

    private Auction auction;

    @Before
    public void before(){
        this.mapper = new DozerBeanMapper(AuctionBuilder.mappings());
        this. auction = MockAuctionFactory.buildAuction(500d);
    }

    @Test
    public void testDtoAuctionMap(){

        RetrieveAuction result = mapper.map(auction,RetrieveAuction.class);

        Assert.assertEquals(result.getId(),auction.getId());
        Assert.assertEquals(result.getPrice(),auction.getPrice());
        Assert.assertEquals(result.getStatus(),auction.getStatus());

        Assert.assertEquals(result.getBuyer(),auction.getBuyer().getUser().getLogin());

        Assert.assertEquals(result.getAuctionObject().getName(),auction.getAuctionObject().getName());
        Assert.assertEquals(result.getAuctionObject().getCount(),auction.getAuctionObject().getCount());

    }
}
