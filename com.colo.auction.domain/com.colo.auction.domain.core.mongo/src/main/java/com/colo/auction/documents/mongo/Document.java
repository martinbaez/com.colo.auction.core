package com.colo.auction.documents.mongo;

public interface Document {

    String getId();
}
