package com.colo.auction.documents.mongo.auction;



import com.colo.auction.documents.mongo.offer.IOffer;
import com.colo.auction.documents.mongo.user.AuctionUser;

import java.util.List;
/**
 * @author martin.baez
 * @date 16/06/19
 */
public interface IAuction {

    IAuctionedObject getAuctionObject();

    IOffer getLowestPriceOffer();

    AuctionUser getBuyer();

   void setBuyer(AuctionUser user);

    Double getPrice();

    AuctionStatus getStatus();

    List<IOffer> getOffers();

    void validateState() throws IllegalStateException;


}
