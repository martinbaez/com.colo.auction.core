package com.colo.auction.documents.mongo.tag;

import com.colo.auction.documents.mongo.AbstractDocument;
import com.colo.auction.documents.mongo.tag.content.TagValueContent;

/**
 * @author martin.baez
 * @date 17/06/19
 */

public class TagValue extends AbstractDocument {

    private TagKey tagKey;

    private TagValueContent value;
}
