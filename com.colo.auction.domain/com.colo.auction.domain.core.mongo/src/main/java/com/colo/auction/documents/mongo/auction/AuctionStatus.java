package com.colo.auction.documents.mongo.auction;

public enum AuctionStatus {
   BOUGHT,
   SUSPENDED,
   OPEN,
   CLOSED

}


