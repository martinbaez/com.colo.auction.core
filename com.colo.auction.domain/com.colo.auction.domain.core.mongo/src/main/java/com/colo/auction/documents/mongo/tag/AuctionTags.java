package com.colo.auction.documents.mongo.tag;

import com.colo.auction.documents.mongo.AbstractDocument;
import com.colo.auction.documents.mongo.auction.Auction;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author martin.baez
 * @date 11/09/19
 */

@Document
@TypeAlias("Tag")
public class AuctionTags extends AbstractDocument {

    private Auction auction;

    private TagValue tagValue;
}
