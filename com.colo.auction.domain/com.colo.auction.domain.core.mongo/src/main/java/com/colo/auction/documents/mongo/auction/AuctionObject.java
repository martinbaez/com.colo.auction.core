package com.colo.auction.documents.mongo.auction;

import com.colo.auction.documents.mongo.AbstractDocument;
import com.colo.auction.documents.mongo.tag.TagValue;

import java.util.HashMap;
import java.util.Map;

/**
 * @author martin.baez
 * @date 16/06/19
 */

public class AuctionObject extends AbstractDocument implements IAuctionedObject {

    private int count;

    private String name;

    private String description;


    private Map<String, TagValue> tags = new HashMap<>();

    public AuctionObject() {
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, TagValue> getTags() {
        return tags;
    }

    public void setTags(Map<String, TagValue> tags) {
        this.tags = tags;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
