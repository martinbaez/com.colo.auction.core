package com.colo.auction.documents.mongo.offer;

public enum OfferStatus {
    SUBMITTED,RECEIVED, WINNER, CLOSED;

}
