package com.colo.auction.documents.mongo.auction;

import com.colo.auction.documents.mongo.AbstractDocument;
import com.colo.auction.documents.mongo.offer.IOffer;
import com.colo.auction.documents.mongo.offer.Offer;
import com.colo.auction.documents.mongo.user.AuctionUser;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * @author martin.baez
 * @date 16/06/19
 */
@Document
@TypeAlias("Auction")
public class Auction extends AbstractDocument implements IAuction {

    @DBRef(lazy = true)
    private List<IOffer> offers = new ArrayList<IOffer>();

    private AuctionUser buyer;

    private AuctionObject auctionObject;

    private Double price;

    private AuctionStatus status;

    public Auction() {
        this.status=AuctionStatus.OPEN;
    }

    @Transient
    public IOffer getLowestPriceOffer() {
        return this.getOffers().stream().min(Comparator.comparing(IOffer::getPrice)).get();
    }

    public void setOffers(List<IOffer> offers) {
        this.offers = offers;
    }

    public AuctionObject getAuctionObject() {
        return auctionObject;
    }

    public void setAuctionObject(AuctionObject auctionObject) {
        this.auctionObject = auctionObject;
    }

    public AuctionUser getBuyer() {
        return buyer;
    }

    public void setBuyer(AuctionUser buyer) {
        this.buyer = buyer;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Auction [id=" + this.getId() + " ]";
    }

    public AuctionStatus getStatus() {
        return status;
    }

    public void setStatus(AuctionStatus status) {
        this.status = status;
    }

    public List<IOffer> getOffers() {
        return offers;
    }

    @Override
    public void validateState() {
        //FIXME in the future buyer should be a mandatory property
       // boolean state = (price > 0) && (status != null) && (auctionObject != null) && (buyer != null);
        boolean state = (price > 0) && (status != null) && (auctionObject != null);

        if (!state){
            throw new IllegalStateException();

        }

    }

    public void addOffer(Offer offer) {
        this.getOffers().add(offer);
    }
}
