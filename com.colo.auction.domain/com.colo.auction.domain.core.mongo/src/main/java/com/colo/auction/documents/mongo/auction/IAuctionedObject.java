package com.colo.auction.documents.mongo.auction;

public interface IAuctionedObject {

   int getCount();

    String getName();


}
