package com.colo.auction.documents.mongo.offer;

import com.colo.auction.documents.mongo.AbstractDocument;
import com.colo.auction.documents.mongo.auction.Auction;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * @author martin.baez
 * @date 16/06/19
 */
@TypeAlias("Offer")
@Document
public class Offer extends AbstractDocument implements IOffer {

    private Double price;

    private Date date;

    private Auction auction;

    private OfferStatus status;

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }


    public Date getDate() {
        return date;
    }


    public void setDate(Date date) {
        this.date = date;
    }


    public OfferStatus getStatus() {
        return status;
    }

    public void setStatus(OfferStatus status) {
        this.status = status;
    }

    public Auction getAuction() {
        return auction;
    }

    public void setAuction(Auction auction) {
        this.auction = auction;
    }
}
