package com.colo.auction.documents.mongo;

import org.springframework.data.annotation.Id;

/**
 * @author martin.baez
 * @date 16/06/19
 */
public class AbstractDocument implements Document {


    @Id
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
            this.id = id;
    }
}
