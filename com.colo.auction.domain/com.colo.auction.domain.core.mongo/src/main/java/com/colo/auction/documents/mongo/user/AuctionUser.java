package com.colo.auction.documents.mongo.user;

import com.colo.auction.documents.mongo.AbstractDocument;
import com.colo.auction.documents.mongo.auction.Auction;
import com.colo.auction.documents.mongo.offer.IOffer;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * @author martin.baez
 * @date 16/06/19
 */
@TypeAlias("AuctionUser")
@Document
public class AuctionUser extends AbstractDocument {

    //Composition rather than inheritance
    private User user;

    private List<IOffer> offersMade = new ArrayList<IOffer>();

    private List<Auction> sales = new ArrayList<Auction>();

    public AuctionUser(User user) {
        this.setUser(user);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    public List<Auction> getSales() {
        return sales;
    }

    public void setSales(List<Auction> sales) {
        this.sales = sales;
    }

    public List<IOffer> getOffersMade() {
        return offersMade;
    }

    public void setOffersMade(List<IOffer> offersMade) {
        this.offersMade = offersMade;
    }
}
