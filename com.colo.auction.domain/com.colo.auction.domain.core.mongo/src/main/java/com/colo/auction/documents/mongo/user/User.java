package com.colo.auction.documents.mongo.user;

import com.colo.auction.documents.mongo.AbstractDocument;

/**
 * @author martin.baez
 * @date 16/06/19
 */

public class User extends AbstractDocument {

    private String login;

    private String surname;

    private String name;

    private String email;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
