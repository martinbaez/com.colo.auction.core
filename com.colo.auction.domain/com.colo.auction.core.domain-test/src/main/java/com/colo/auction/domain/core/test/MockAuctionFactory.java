package com.colo.auction.domain.core.test;

import com.colo.auction.documents.mongo.auction.Auction;
import com.colo.auction.documents.mongo.auction.AuctionObject;
import com.colo.auction.documents.mongo.auction.IAuction;
import com.colo.auction.documents.mongo.offer.IOffer;
import com.colo.auction.documents.mongo.offer.Offer;
import com.colo.auction.documents.mongo.user.AuctionUser;

import java.util.Random;
import java.util.UUID;

/**
 * @author martin.baez
 * @date 17/06/19
 */

public class MockAuctionFactory {

    private static String[] objects = {"Juego de Sillones", "Kit de Supervivencia", "Chevrolet Cruze 2010", "Vino", "Cathering para fiesta infantil", "200 kilos de pan",
    "40 toneladas de escombro", "Juego de Copas", "Equipo de futbol(camiseta, pantalón y medias)"} ;


    public static Auction buildAuction(Double price) {

        Auction auction = new Auction();
        auction.setPrice(price);
     //   auction.setId(MockAuctionFactory.createTenantId());
        auction.setAuctionObject(buildAuctionedObject());

        auction.setBuyer(MockUserFactory.buildUser());

        return auction;

    }

    public static String createTenantId() {
        return UUID.randomUUID().toString();
    }

    public static IOffer buildOffer(IAuction auction, Double amount, AuctionUser seller){
        Offer offer = new Offer();
        offer.setPrice(amount);
        offer.setId(MockAuctionFactory.createTenantId());

        seller.getOffersMade().add(offer);
        return offer;
    }


    public static AuctionObject buildAuctionedObject(){

        AuctionObject auctionObject = new AuctionObject();

        int objectsIdx = new Random().nextInt(objects.length -1);

        auctionObject.setName(objects[objectsIdx]);
        auctionObject.setDescription(objects[objectsIdx]);
       // auctionObject.setId(createTenantId());
        auctionObject.setCount(new Random().nextInt(60));

        return auctionObject;
    }

    public static Auction buildAuctionWithSomeOffer() {
        AuctionUser buyer = MockUserFactory.buildUser();

        Auction auction = new Auction();
        auction.setBuyer(buyer);
        auction.setPrice(new Random().nextDouble());
        auction.setId(UUID.randomUUID().toString());


        AuctionUser seller = MockUserFactory.buildUser();
        IOffer bestOffer = MockAuctionFactory.buildOffer(auction,new Random().nextDouble(), seller);
        auction.getOffers().add(bestOffer);
        seller.getOffersMade().add(bestOffer);

        IOffer worstOffer = MockAuctionFactory.buildOffer(auction,new Random().nextDouble(), seller);
        auction.getOffers().add(worstOffer);
        seller.getOffersMade().add(worstOffer);

        AuctionUser seller2 =  MockUserFactory.buildUser();
        IOffer middleOffer =  MockAuctionFactory.buildOffer(auction,new Random().nextDouble(), seller2);
        seller2.getOffersMade().add(middleOffer);

        auction.getOffers().add(middleOffer);


        return auction;
    }
}
