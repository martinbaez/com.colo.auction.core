package com.colo.auction.domain.core.test;

import com.colo.auction.documents.mongo.user.AuctionUser;
import com.colo.auction.documents.mongo.user.User;

import java.util.Random;

/**
 * @author martin.baez
 * @date 3/08/19
 */

public class MockUserFactory {

    private static String[] names = {"Martin", "Hector", "Jorge", "Salustiano","John", "Belisario", "Antonio", "Juan"};

    private static String[] surnames = {"Gonzales", "Perez", "Calori", "Berreda","Ayala", "Carpela", "Bossio", "Anaya"};

    private static String[] mailDomains = {"yahoo.com","gmail.com", "outlook.com", "topmail.com","mail.com", "asd.net"};

    public static AuctionUser buildUser() {
        Random random = new Random();

        User user = new User();

        int nameIndex = random.nextInt(names.length-1);

        int surnameIndex = random.nextInt(surnames.length-1);

        String name = names[nameIndex];

        String surname = surnames[surnameIndex];

        int emailServverIndex = random.nextInt(mailDomains.length-1);

        user.setName(name);
        user.setSurname(surname);
        user.setLogin(name + "." + surname);

        user.setEmail(user.getLogin() + "@" + mailDomains[emailServverIndex]);

        AuctionUser auctionUser = new AuctionUser(user);

        auctionUser.setUser(user);

        return auctionUser;

    }
}
