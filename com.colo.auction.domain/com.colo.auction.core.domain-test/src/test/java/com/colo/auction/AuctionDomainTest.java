package com.colo.auction;

import com.colo.auction.documents.mongo.auction.IAuction;
import com.colo.auction.documents.mongo.offer.IOffer;
import com.colo.auction.documents.mongo.user.AuctionUser;
import com.colo.auction.domain.core.test.MockAuctionFactory;
import com.colo.auction.domain.core.test.MockUserFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author martin.baez
 * @date 16/06/19
 */

public class AuctionDomainTest {

    private IAuction auction;

    private IOffer lowesPriceOffer;

    @Before
    public void before(){
        auction  = MockAuctionFactory.buildAuction(10d);

        AuctionUser buyer = MockUserFactory.buildUser();
        auction.setBuyer(buyer);

        AuctionUser seller =  MockUserFactory.buildUser();

        IOffer bestOffer = MockAuctionFactory.buildOffer(auction,10d, seller);
        this.lowesPriceOffer = bestOffer;
        auction.getOffers().add(bestOffer);
        seller.getOffersMade().add(bestOffer);

        IOffer worstOffer = MockAuctionFactory.buildOffer(auction,100d, seller);
        auction.getOffers().add(worstOffer);
        seller.getOffersMade().add(worstOffer);

        AuctionUser otherSeller = MockUserFactory.buildUser();
        IOffer middleOffer =  MockAuctionFactory.buildOffer(auction,50d, otherSeller);
        otherSeller.getOffersMade().add(middleOffer);


        auction.getOffers().add(middleOffer);

    }

    @Test
    public void testGetLowestPriceOffer(){

        IOffer offer  = this.auction.getLowestPriceOffer();

        Assert.assertEquals(this.lowesPriceOffer,offer);

    }



}
