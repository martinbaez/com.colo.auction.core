package com.colo.auction.db.def;

import com.colo.auction.dataSource.core.mongo.AuctionRepository;
import com.colo.auction.dataSource.core.mongo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * @author martin.baez
 * @date 3/08/19
 */
@SpringBootApplication(scanBasePackageClasses = {BusinessDataCommand.class},exclude = {DataSourceAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
@EnableMongoRepositories(basePackageClasses = {AuctionRepository.class, UserRepository.class})
public class BusinessDataApp implements CommandLineRunner{

    /*
    *
    * IMPORTANT SET THIS:
    *
    * -Dspring.profiles.active=dev
    *
     */


    @Autowired
    private BusinessDataCommand businessDataCommand;

    public static void main(String[] args) {

        System.setProperty("spring.profiles.active","dev");
        SpringApplication.run(BusinessDataApp.class, args);
    }


    @Override
    public void run(String... args) throws Exception {
        this.businessDataCommand.execute();
    }
}
