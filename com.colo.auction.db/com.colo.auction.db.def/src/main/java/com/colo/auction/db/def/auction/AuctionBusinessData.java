package com.colo.auction.db.def.auction;

import com.colo.auction.dataSource.core.mongo.AuctionRepository;
import com.colo.auction.dataSource.core.mongo.UserRepository;
import com.colo.auction.db.def.AbstractBusinessDataCommand;
import com.colo.auction.documents.mongo.auction.Auction;
import com.colo.auction.documents.mongo.user.AuctionUser;
import com.colo.auction.domain.core.test.MockAuctionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Random;

/**
 * @author martin.baez
 * @date 3/08/19
 */
@Component
public class AuctionBusinessData extends AbstractBusinessDataCommand {

    private static final int AUCTIONS = 1 ;
    @Autowired
    private AuctionRepository repository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public void doExecute() {

        for (int i = 0; i < AUCTIONS ; i++) {

            Auction auction = MockAuctionFactory.buildAuction(500d);

            List<AuctionUser> users = userRepository.findAll();

            int idx = new Random().nextInt(users.size() - 1);
            AuctionUser user = users.get(idx);

            //TODO: DO NOT CREATE IT, TAKE A RANDOM USER FROM USERS COLLECTION
            auction.setBuyer(user);

            this.repository.save(auction);
        }


    }



    @Override
    protected MongoRepository getRepository() {
        return (MongoRepository) this.repository;
    }
}
