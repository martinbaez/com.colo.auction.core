package com.colo.auction.db.def;

import com.colo.auction.dataSource.core.mongo.UserRepository;
import com.colo.auction.db.def.auction.AuctionBusinessData;
import com.colo.auction.db.def.user.UserBusinessData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author martin.baez
 * @date 2/08/19
 */
@Component
public class BusinessDataCommand  {

    @Autowired
    private UserBusinessData userBusinessData;

    @Autowired
    private AuctionBusinessData auctionBusinessData;

    public void execute() {

        this.userBusinessData.execute();

        this.auctionBusinessData.execute();
    }


}
