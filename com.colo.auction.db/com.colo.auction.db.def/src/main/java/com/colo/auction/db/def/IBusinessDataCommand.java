package com.colo.auction.db.def;

public interface IBusinessDataCommand {

   void clean();

   void execute();

}
