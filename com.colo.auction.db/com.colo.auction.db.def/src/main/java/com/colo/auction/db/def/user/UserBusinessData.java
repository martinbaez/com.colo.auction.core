package com.colo.auction.db.def.user;

import com.colo.auction.dataSource.core.mongo.UserRepository;
import com.colo.auction.db.def.AbstractBusinessDataCommand;
import com.colo.auction.documents.mongo.user.AuctionUser;
import com.colo.auction.domain.core.test.MockUserFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

/**
 * @author martin.baez
 * @date 3/08/19
 */
@Component
public class UserBusinessData extends AbstractBusinessDataCommand {

     private static int USERS =  100;

     @Autowired
     private UserRepository repository;

    @Override
    public void doExecute() {


        for (int i = 0; i < USERS ; i++) {

            AuctionUser user = MockUserFactory.buildUser();

            this.repository.save(user);
        }

    }

    @Override
    protected MongoRepository<Object, Object> getRepository() {
        return  (MongoRepository) this.repository;
    }
}
