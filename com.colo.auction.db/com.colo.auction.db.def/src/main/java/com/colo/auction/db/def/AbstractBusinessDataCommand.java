package com.colo.auction.db.def;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author martin.baez
 * @date 3/08/19
 */

public abstract class AbstractBusinessDataCommand implements IBusinessDataCommand {


    @Override
    public void clean() {
        this.getRepository().deleteAll();
    }

    protected abstract MongoRepository getRepository();

    @Override
    public final void execute() {
        this.clean();

        this.doExecute();
    }

    protected abstract void doExecute();
}
